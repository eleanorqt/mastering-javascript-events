// Import the validate function from email-validator.js
import { validate } from './src/email-validator.js';

// Function to create and style the "Join Our Program" section
function createJoinOurProgramSection() {
    // Create a container div for the section
    const joinProgramSection = document.createElement('div');
    joinProgramSection.classList.add('join-program-section');

    // Create and set attributes for the section title
    const sectionTitle = document.createElement('h2');
    sectionTitle.textContent = 'Join Our Program';
    joinProgramSection.appendChild(sectionTitle);

    // Create and set attributes for the description paragraph
    const description = document.createElement('p');
    description.textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae lectus.';
    joinProgramSection.appendChild(description);

    // Create the form element
    const form = document.createElement('form');

    // Create and set attributes for the email input field
    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.id = 'email-input';
    emailInput.placeholder = 'Enter your email';
    emailInput.required = true;
    form.appendChild(emailInput);

    // Create and set attributes for the subscribe button
    const subscribeButton = document.createElement('button');
    subscribeButton.type = 'submit';
    subscribeButton.textContent = 'Subscribe';
    form.appendChild(subscribeButton);

    // Append the form to the section
    joinProgramSection.appendChild(form);

    // Append the whole section to the main container element in the HTML
    const mainContainer = document.querySelector('#app-container');
    mainContainer.appendChild(joinProgramSection);
}

// Event listener for the DOMContentLoaded event
document.addEventListener('DOMContentLoaded', createJoinOurProgramSection);

// Event listener for the form submission
document.querySelector('form').addEventListener('submit', function (event) {
    event.preventDefault();
    const emailInput = document.getElementById('email-input').value;
    console.log('Entered email:', emailInput);
});

// Get the email input element
const emailInput = document.getElementById('email-input');

// Add an event listener to the email input to save its value to localStorage
emailInput.addEventListener('input', function () {
    const email = emailInput.value;
    localStorage.setItem('subscriptionEmail', email);
});

// Check if there is a stored email in localStorage
const storedEmail = localStorage.getItem('subscriptionEmail');

// If there is a stored email, populate the email input with its value
if (storedEmail) {
    emailInput.value = storedEmail;
}

// Get the form element
const form = document.querySelector('form');

// Get the "Subscribe" button
const subscribeButton = form.querySelector('button[type="submit"]');

// Add an event listener to the form submission
form.addEventListener('submit', function (event) {
    event.preventDefault();

    // Get the current email input value
    const email = emailInput.value;

    // Validate the email using the validate function (assuming you have implemented the validate function)
    const isValidEmail = validate(email);

    // If the email is valid, handle the unsubscribe action
    if (isValidEmail) {
        // Check if the email input is currently hidden (meaning the user is subscribed)
        const isSubscribed = emailInput.style.display === 'none';

        // Toggle the subscription status
        if (isSubscribed) {
            // If already subscribed, unsubscribe
            emailInput.style.display = 'block';
            subscribeButton.textContent = 'Subscribe';
            localStorage.removeItem('subscriptionEmail');
        } else {
            // If not subscribed, subscribe
            emailInput.style.display = 'none';
            subscribeButton.textContent = 'Unsubscribe';
        }
    }
});
