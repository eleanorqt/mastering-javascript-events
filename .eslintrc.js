module.exports = {
    parserOptions: {
        ecmaVersion: 2021,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        browser: true,
        node: true,
    },
    plugins: ['react'],
    extends: ['eslint:recommended', 'plugin:react/recommended'],
    rules: {
        // Override rules here as needed
        'no-unused-vars': 'off', // Example: Disable no-unused-vars rule
        // Add other rule overrides
    },
    settings: {
        react: {
            version: 'detect', // or specify your React version, e.g., "17.0.1"
        },
    },
};
