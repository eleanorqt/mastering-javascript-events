// join-us-section.js

// Private function to create and style the "Join Our Program" section
function createJoinOurProgramSection() {
    // Create a container div for the section
    const joinProgramSection = document.createElement("div");
    joinProgramSection.classList.add("join-program-section");

    // Create and set attributes for the section title
    const sectionTitle = document.createElement("h2");
    sectionTitle.textContent = "Join Our Program";
    joinProgramSection.appendChild(sectionTitle);

    // Create and set attributes for the description paragraph
    const description = document.createElement("p");
    description.textContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae lectus.";
    joinProgramSection.appendChild(description);

    // Create the form element
    const form = document.createElement("form");

    // Create and set attributes for the email input field
    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.id = "email-input";
    emailInput.placeholder = "Enter your email";
    emailInput.required = true;
    form.appendChild(emailInput);

    // Create and set attributes for the subscribe button
    const subscribeButton = document.createElement("button");
    subscribeButton.type = "submit";
    subscribeButton.textContent = "Subscribe";
    form.appendChild(subscribeButton);

    // Append the form to the section
    joinProgramSection.appendChild(form);

    // Append the whole section to the main container element in the HTML
    const mainContainer = document.querySelector("#app-container");
    mainContainer.appendChild(joinProgramSection);
}

// Public method to initialize the module
function init() {
    createJoinOurProgramSection();
}

// Export the public method
export { init };

// join-us-section.js (Factory Method implementation)

// Factory function to create standard program section
function createStandardProgramSection() {
    const section = document.createElement("section");
    section.classList.add("join-program-section");

    const title = document.createElement("h2");
    title.textContent = "Join Our Program";
    section.appendChild(title);

    const description = document.createElement("p");
    description.textContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae lectus.";
    section.appendChild(description);

    const form = document.createElement("form");
    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.id = "email-input";
    emailInput.placeholder = "Enter your email";
    emailInput.required = true;
    form.appendChild(emailInput);

    const subscribeButton = document.createElement("button");
    subscribeButton.type = "submit";
    subscribeButton.textContent = "Subscribe";
    form.appendChild(subscribeButton);

    section.appendChild(form);

    // Method to remove the created section from the page
    section.remove = function () {
        section.parentElement.removeChild(section);
    };

    return section;
}

// Factory function to create advanced program section
function createAdvancedProgramSection() {
    const section = createStandardProgramSection();
    const title = section.querySelector("h2");
    const subscribeButton = section.querySelector("button");

    title.textContent = "Join Our Advanced Program";
    subscribeButton.textContent = "Subscribe to Advanced Program";

    return section;
}

// Factory method to create different types of programs
function SectionCreator() { }

SectionCreator.prototype.create = function (type) {
    if (type === "standard") {
        return createStandardProgramSection();
    } else if (type === "advanced") {
        return createAdvancedProgramSection();
    } else {
        throw new Error("Invalid program type");
    }
};

export default SectionCreator;
