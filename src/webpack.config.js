const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';

    return {
        entry: './src/index.js',
        mode: isProduction ? 'production' : 'development',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: isProduction ? 'bundle.min.js' : 'bundle.js',
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    },
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.(png|jpe?g|gif)$/i,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192,
                                name: 'images/[name].[ext]',
                            },
                        },
                    ],
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './index.html',
            }),
            new CopyWebpackPlugin({
                patterns: [
                    { from: 'assets/images', to: 'images' },
                ],
            }),
        ],
        devServer: {
            hot: true,
        },
        optimization: {
            minimize: isProduction,
            minimizer: [new TerserWebpackPlugin()],
        },
    };
};
