// email-validator.js

// Module scoped variable
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

// Export the function to validate the email
export function validate(email) {
    // Extract the domain from the email
    const domain = email.split('@')[1];

    // Check if the domain is in the list of valid endings
    return VALID_EMAIL_ENDINGS.includes(domain);
}